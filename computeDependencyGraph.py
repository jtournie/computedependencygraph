'''
  This script aims at computing the WinCC OA dependency
  graph of a given component. The component is identified
  by its XML description file. The script relies on the assumption
  that the component name is the same as the name of its XML description file
'''

import argparse
import xml.etree.ElementTree as ET
import os
import sys, errno
import graphviz
import datetime

#-------------------------------------------------------
class Component:
  #list all visited component to stop the recursion when exploring the XML files
  visited_components = [] 

  def __init__(self):
    self.name = ""
    self.file = ""
    self.version = ""
    self.reqComp = []
    self.includeComp = []

  def printInfo(self):
    print(self.name, self.version, self.file)

#-------------------------------------------------------
def compute_dependency(componentFileName):
  if not os.path.isfile(componentFileName):
    print('Component file description does not exist! '+ componentFileName)
    return

  tree = ET.parse(componentFileName)
  root = tree.getroot()
  reqCompTags = root.findall('required')
  includeCompTags = root.findall('includeComponent')

  component =  Component()
  component.file = componentFileName
  component.name = root.find('name').text
  component.version = root.find('version').text

  if component.name in component.visited_components:
    return component

  component.visited_components.append(component.name)

  for compTag in reqCompTags:
    reqComponentNameVersion = compTag.text.split('=')
    reqComponentName = reqComponentNameVersion[0]
    reqComponentVersion = reqComponentNameVersion[1] if len(reqComponentNameVersion) == 2 else ""
    component.reqComp.append(compute_dependency(os.path.dirname(componentFileName)+'/'+reqComponentName+'.xml'))

  for compTag in includeCompTags:
    reqComponentName = compTag.text.replace('./','').replace('.xml','')
    component.includeComp.append(compute_dependency(os.path.dirname(componentFileName)+'/'+reqComponentName+'.xml'))

  return component

#-------------------------------------------------------
def getShapeNode(componentName):
  if componentName.startswith('un'):
    return 'circle'
  if componentName.startswith('fw'):
    return 'oval'
  return 'egg'

#-------------------------------------------------------
def getColorNode(componentName):
  if componentName.startswith('un'):
    return '3'
  if componentName.startswith('fw'):
    return '2'
  return '1'

#-------------------------------------------------------
def createNode(dot, nodeName):
  dot.attr('node', shape=getShapeNode(nodeName))
  dot.attr('node', color=getColorNode(nodeName))
  dot.attr('node', bgcolor=getColorNode(nodeName))
  dot.node(nodeName)


#-------------------------------------------------------
def generate_graphviz_tree(dot, component):
  createNode(dot, component.name)
  for reqComp in component.reqComp:
    createNode(dot, reqComp.name)
    dot.attr('edge', style='solid')
    dot.edge(component.name, reqComp.name)
    generate_graphviz_tree(dot, reqComp)

  for includeComp in component.includeComp:
    createNode(dot, includeComp.name)
    dot.attr('edge', style='dashed')
    dot.edge(component.name, includeComp.name)
    generate_graphviz_tree(dot, includeComp)
  


#-------------------------------------------------------
def generate_graphviz(rootComponent, outputFile, outputFormat):
  dot = graphviz.Digraph(comment='Component graph diagram for '+rootComponent.name, filename=outputFile, format=outputFormat, engine='dot')
  dot.attr(label='Component dependency graph for '+rootComponent.name+' version '+rootComponent.version + ' generated on '+ str(datetime.date.today()))
  dot.attr(labelloc='t')
  dot.attr(fontsize='30')

  dot.attr('node', style='filled')
  dot.attr('node', colorscheme='gnbu4')

  dot.attr('node', shape='doublecircle')
  dot.attr('node', color='4')
  dot.attr('node', bgcolor='4')
  dot.node(rootComponent.name)

  generate_graphviz_tree(dot, rootComponent)

  dot.render(cleanup=True)

#-------------------------------------------------------
def main(componentXmlFileName, outputFile, outputFormat):
  dependencyGraph = compute_dependency(componentXmlFileName) 

  gv = generate_graphviz(dependencyGraph, outputFile, outputFormat)


#-------------------------------------------------------
if __name__ == '__main__':
  #check args
  parser = argparse.ArgumentParser('''
                                  Compute the component dependency of the component given as input.
                                  The component input is identified by its XML description file. The
                                  script assumes that each component name and its description file name are the same.
                                  It also assumes that all XML files are located in the same folder.
                                  E.g.: python3 computeDependencyGraph.py -compXmlFile /tmp/unicos-framework_8.6.1/unicos-framework.xml -outputFile /tmp/graph.png --outputFormat png
                                  ''')
  parser.add_argument('-compXmlFile', required=True, action='store', type=str, help='XML component description file for which the dependency needs to be computed')
  parser.add_argument('-outputFile', required=True, action='store', type=str, help='Output file containing the dependency graph. Overwrite the file if it is already existing. No extension needed.')
  parser.add_argument('--outputFormat', action='store', type=str, choices=['pdf','png','dot', 'svg','plain-ext', 'json', 'jpeg','gif',], help='Format of the output file (pdf by default).', default='pdf')
  
  args = parser.parse_args()

  if not os.path.isfile(args.compXmlFile):
    print('Component description file does not exist! '+args.compXmlFile)
    print('Process aborted.')
    sys.exit(errno.ENOENT)

  splitFileNameExt = os.path.splitext(args.outputFile)
  
  if splitFileNameExt[1] and splitFileNameExt[1] != '.'+args.outputFormat:
    print('Output file extension differs from output file format.')
    print('Either remove file extension or change it to the output file format.')
    print('Process aborted.')
    sys.exit(errno.EPERM)


  main(args.compXmlFile, splitFileNameExt[0], args.outputFormat) 
